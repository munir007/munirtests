<?php

namespace App\Console\Commands;

use App\Mail\EventStartReminder;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EventStartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:eventstart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command for email notification for event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $intervel1 = Carbon::now()->subMinutes(9);
        $intervel2 = Carbon::now()->subMinutes(10);

        $users = User::with(['tickets'=>function($q) use ($intervel1,$intervel2){
            $q->where('start_time','<=',$intervel1)->where('start_time','>=',$intervel2)->with('events:id,title')->select(['id','user_id','event_id','start_time']);
        }])->get(['id','username','email']);

        foreach ($users as $user){
            if (empty($user->tickets)){
                continue;
            }

            $email = new EventStartReminder($user);
            Mail::to($user->email)->send($email);
        }
    }
}
