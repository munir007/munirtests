<?php

namespace App\Http\Controllers;

use App\Models\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $events = Events::with(['tickets'=>function($q) use ($user){
            $q->where('user_id',$user->id);
        }])->get();

        return view('events', compact('events'));
    }
}
