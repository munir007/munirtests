<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use App\Models\Events;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Laravel\Ui\AuthRouteMethods;

class TicketController extends Controller
{
    public function index()
    {
        $tickets = Ticket::where('user_id',Auth::id())->get();

        return view('tickets', compact('tickets'));
    }

    public function book($id)
    {
        $event = Events::find($id);

        if (empty($event)) {
            return redirect('/events')
                ->withErrors('Event is invalid')
                ->withInput();
        }
        return view('ticketbooking', compact('event'));
    }

    public function ticketbooking(Request $request)
    {
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'user_id' => 'required|integer|exists:users,id',
            'event_id' => 'required|integer|exists:events,id',
            'name' => 'required',
            'phone' => 'required|min:10|numeric',
            'start_time' => 'required|date_format:Y-m-d H:i:s|after_or_equal:now',
            'end_time' => 'required|date_format:Y-m-d H:i:s|after:start_time',
            'payment_method' => 'required|in:cash,card',
        ]);

        if ($validator->fails()) {
            return redirect('/book/'.$inputs['event_id'])
                ->withErrors($validator)
                ->withInput();
        }

        unset($inputs['_token']);

        $event = Ticket::create($inputs);

        if ($event){
            try {
                $job = (new SendEmailJob(Auth::user()))->delay(10);

                $this->dispatch($job);
            }catch (Exception $exception){}
        }

        return redirect()->route('events');
    }

}
