<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventsController extends Controller
{
    public function index()
    {
        $events = Events::all();

        return view('admin.events.index', compact('events'));
    }

    public function add()
    {
        $events = Events::all();

        return view('admin.events.add', compact('events'));
    }

    public function create(Request $request)
    {
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'title' => 'required|unique:events|max:255',
            'max_available_tickets' => 'required|integer',
            'start_time' => 'required|date_format:Y-m-d H:i:s|after:'.date("Y-m-d", strtotime('tomorrow')),
            'end_time' => 'required|date_format:Y-m-d H:i:s|after:start_time',
        ]);

        if ($validator->fails()) {
            return redirect('admin/events/add')
                ->withErrors($validator)
                ->withInput();
        }
        unset($inputs['_token']);

        $event = Events::create($inputs);

        return redirect()->route('admin.events');
    }
}
