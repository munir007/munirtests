<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('/admin')->name('admin.')->namespace('App\Http\Controllers\Admin')->group(function(){
    Route::namespace('Auth')->group(function(){

        //Login Routes
        Route::get('/','LoginController@showLoginForm');
        Route::get('/login','LoginController@showLoginForm')->name('login');
        Route::post('/login','LoginController@login');
        Route::post('/logout','LoginController@logout')->name('logout');

    });

    Route::group(['middleware' => 'auth'], function() {
        Route::get('/dashboard','HomeController@index')->name('home');
        Route::get('/events', 'EventsController@index')->name('events');
        Route::get('/events/add', 'EventsController@add')->name('events.add');
        Route::post('/events/create', 'EventsController@create')->name('events.create');
        Route::get('/tickets', 'TicketController@index')->name('tickets');
    });
});

Route::group(['namespace' => 'App\Http\Controllers','middleware' => 'auth'], function() {
//Route::namespace('App\Http\Controllers')->group(function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/events', 'EventsController@index')->name('events');
    Route::get('/tickets', 'TicketController@index')->name('tickets');
    Route::get('/book/{id}', 'TicketController@book');
    Route::post('/ticket-booking', 'TicketController@ticketbooking')->name('ticketbooking');
});
