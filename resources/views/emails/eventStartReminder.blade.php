@component('mail::message')
Hello {{$user->username}}

Your event {{$user->tickets->events->title}} will be start in 10 min.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
