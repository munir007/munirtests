@extends('layouts.app')
@section('header')
    <link rel="stylesheet" type="text/css" media="screen"
          href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <style>

    </style>
@endsection
@section('content')
    <div class="container">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class='error'>{{$error}}</div>
            @endforeach
        @endif
        <form method="post" action="{{route('ticketbooking')}}">
            @csrf
            <input type="hidden" name="user_id" value="{{old('user_id')??Auth::id()}}">
            <input type="hidden" name="event_id" value="{{old('event_id')??request()->route('id')}}">
            <h2>Ticket Booking</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" placeholder="name" id="name" name="name" value="{{ old('name') }}">
                    </div>
                </div>
                <!--  col-md-6   -->

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" placeholder="" id="phone" name="phone" value="{{ old('phone') }}">
                    </div>


                </div>
            </div>


            <div class="row">
                <!--  col-md-6   -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="start_time">Start Time</label>
                        <input type="text" class="form-control" placeholder="Y-m-d H:i" id="start_time" name="start_time" value="{{ old('start_time') }}">
                    </div>
                </div>
            {{--<div style="overflow:hidden;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="datetimepicker12"></div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!--  col-md-6   -->

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="end_time">End Time</label>
                        <input type="text" class="form-control" placeholder="Y-m-d H:i" id="end_time" name="end_time" value="{{ old('end_time') }}">
                    </div>
                </div>
                <!--  col-md-6   -->
            </div>
            <!--  row   -->

            <div class="row">
                <!--  col-md-6   -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="payment_method">Start Time</label>
                        <select class="form-control" id="payment_method" name="payment_method" >
                            <option value="cash" {{ (old('payment_method')=='cash')?'selected':'' }}>Cash</option>
                            <option value="card" {{ (old('payment_method')=='card')?'selected':'' }}>Card</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>
@endsection
@section('footer')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker12').datetimepicker({
                inline: true,
                sideBySide: true
            });
        });
    </script>
@endsection
