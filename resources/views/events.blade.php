@extends('layouts.app')
@section('header')
    <style>
        body {margin:2em;}
    </style>
@endsection
@section('content')

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class='error'>{{$error}}</div>
        @endforeach
    @endif
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Title</th>
        <th>Max Available Tickets</th>
        <th>Start Time</th>
        <th>End Time</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{{$event->title}}</td>
            <td>{{$event->max_available_tickets}}</td>
            <td>{{$event->start_time}}</td>
            <td>{{$event->end_time}}</td>
            <td>
                @if(empty($event->tickets))
                <a class="btn btn-success" style="float:left;margin-right:20px;" href="{{ url('book/'.$event->id) }}">Book Ticket</a>
                @else
                    <span class="badge badge-info">Booked</span>
                @endif
            </td>
        </tr>
    @endforeach
        @if($events->isEmpty())
            <tr>
                <td colspan="5">No record</td>
            </tr>
        @endif
    </tbody>
</table>
@endsection
@section('footer')
    <script>
        $(document).ready(function() {
            //Only needed for the filename of export files.
            //Normally set in the title tag of your page.
            document.title='Simple DataTable';
            // DataTable initialisation
            $('#example').DataTable(
                {
                    "dom": '<"dt-buttons"Bf><"clear">lirtp',
                    "paging": true,
                    "autoWidth": true,
                    "buttons": [
                        'colvis',
                        'copyHtml5',
                        'csvHtml5',
                        'excelHtml5',
                        'pdfHtml5',
                        'print'
                    ]
                }
            );
        });
    </script>
@endsection
