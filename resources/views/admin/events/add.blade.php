@extends('admin.layouts.app')
@section('header')
    <link rel="stylesheet" type="text/css" media="screen"
          href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <style>

    </style>
@endsection
@section('content')
    <div class="container">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class='error'>{{$error}}</div>
            @endforeach
        @endif
        <form method="post" action="{{route('admin.events.create')}}">
            @csrf
            <h2>Add Events</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" placeholder="title" id="title" name="title" value="{{ old('title') }}">
                    </div>
                </div>
                <!--  col-md-6   -->

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="max_available_tickets">Max Available Tickets</label>
                        <input type="number" class="form-control" placeholder="" id="max_available_tickets" name="max_available_tickets" value="{{ old('max_available_tickets') }}">
                    </div>


                </div>
            </div>


            <div class="row">
                <!--  col-md-6   -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="start_time">Start Time</label>
                        <input type="text" class="form-control" placeholder="Y-m-d H:i" id="start_time" name="start_time" value="{{ old('start_time') }}">
                    </div>
                </div>
            {{--<div style="overflow:hidden;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="datetimepicker12"></div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!--  col-md-6   -->

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="end_time">End Time</label>
                        <input type="text" class="form-control" placeholder="Y-m-d H:i" id="end_time" name="end_time" value="{{ old('end_time') }}">
                    </div>
                </div>
                <!--  col-md-6   -->
            </div>
            <!--  row   -->

            <button type="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>
@endsection
@section('footer')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker12').datetimepicker({
                inline: true,
                sideBySide: true
            });
        });
    </script>
@endsection
