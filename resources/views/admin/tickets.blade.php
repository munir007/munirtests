@extends('admin.layouts.app')
@section('header')
    <style>
        body {margin:2em;}
    </style>
@endsection
@section('content')

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class='error'>{{$error}}</div>
        @endforeach
    @endif
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Name</th>
            <th>Phone</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Payment Method</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tickets as $ticket)
            <tr>
                <td>{{$ticket->name}}</td>
                <td>{{$ticket->phone}}</td>
                <td>{{$ticket->start_time}}</td>
                <td>{{$ticket->end_time}}</td>
                <td>{{$ticket->payment_method}}</td>
            </tr>
        @endforeach
        @if($tickets->isEmpty())
            <tr>
                <td colspan="5">No record</td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
@section('footer')
    <script>
        $(document).ready(function() {
            //Only needed for the filename of export files.
            //Normally set in the title tag of your page.
            document.title='Simple DataTable';
            // DataTable initialisation
            $('#example').DataTable(
                {
                    "dom": '<"dt-buttons"Bf><"clear">lirtp',
                    "paging": true,
                    "autoWidth": true,
                    "buttons": [
                        'colvis',
                        'copyHtml5',
                        'csvHtml5',
                        'excelHtml5',
                        'pdfHtml5',
                        'print'
                    ]
                }
            );
        });
    </script>
@endsection
